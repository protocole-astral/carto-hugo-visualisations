/* Script */ 

function get_entries(json,titre_value,obj) {
  let values = {};
  let names = {};
  for (var i in obj) {
    values[obj[i]] = values[obj[i]] == undefined ? 1 : values[obj[i]] + 1;
    names[obj[i]] = names[obj[i]] == undefined ? [i] : names[obj[i]].concat([i]);
  }
  let entries = Object.entries(values);
  entries.reverse();
  if (entries.length < 5) return;
  places = [];
  places_solo = [];
  places_10 = [];
  while (entries.length) {
    let entry = entries.shift();
    console.log(names[entry[0]].length,values[entry[0]]);
    if (entry[0] > 9) {
      for (var i = 0; i < names[entry[0]].length; i++) {
        place_histogramme(names[entry[0]][i],json);
        places_10.push(names[entry[0]][i]);
      }
    } else if (entry[0] == 1) {
      for (var i = 0; i < names[entry[0]].length; i++) {
        places_solo.push(names[entry[0]][i]);
      }
    } else {
      for (var i = 0; i < names[entry[0]].length; i++) {
        places.push(names[entry[0]][i]);
      }
    }
  }
  console.log(places_solo,places);
  for (var i = 0; i < places.length; i++) {
    console.log(places_solo.includes(places[i]));
  }
  var br = document.createElement("br");
  document.body.appendChild(br);
  var br = document.createElement("br");
  document.body.appendChild(br);
  var p = document.createElement("p");
  p.textContent = "Dans un souci de lisibilité du document, seuls les lieux au moins cités dix fois sont représentés individuellement. Les trois derniers graphiques de cette partie présentent le cumul d'occurrences de plusieurs lieux dans l'œuvre. Ceux cités dix fois ou plus pour le premier, ceux cités entre deux et neuf fois pour le deuxième et ceux cités une seule fois pour le dernier";
  document.body.appendChild(p);
  places_histogramme("Lieux cités plus de dix fois",places_10,json);
  places_histogramme("Lieux cités entre neuf et deux fois",places,json);
  places_histogramme("Lieux cités une seule fois",places_solo,json);
}

function occ_histogramme(titre_value,obj,legend) {
  let histogramme = document.createElement("section");
  let titre = document.createElement("h3");
  titre.textContent = titre_value;
  histogramme.appendChild(titre);
  let bars_values = {};
  let bars_names = {};
  for (var i in obj) {
    bars_values[obj[i]] = bars_values[obj[i]] == undefined ? 1 : bars_values[obj[i]] + 1;
    bars_names[obj[i]] = bars_names[obj[i]] == undefined ? [i] : bars_names[obj[i]].concat([i]);
  }
  legend = document.createElement('p');
  legend.className = "legend";
  let bar_occ_value_legend = document.createElement("span");
      bar_occ_value_legend.style.color = "mediumaquamarine";
      bar_occ_value_legend.textContent = "Occurrences par lieu, ";
  let bar_places_value_legend = document.createElement("span");
      bar_places_value_legend.style.color = "darksalmon";
      bar_places_value_legend.textContent = "lieux par occurrence, ";
  let bar_places_legend = document.createElement("span");
      bar_places_legend.textContent = "lieu";
  legend.appendChild(bar_occ_value_legend);
  legend.appendChild(bar_places_value_legend);
  legend.appendChild(bar_places_legend);
  histogramme.appendChild(legend);
  let entries = Object.entries(bars_values);
  entries.reverse();
  if (entries.length < 5) return;
  while (entries.length) {
    let entry = entries.shift();
    let bar = document.createElement("div");
        bar.className = "bar";
        bar.style.display = "flex";
    let bar_places_value = document.createElement("div");
        bar_places_value.style.width = entry[1]*3+'px';
        bar_places_value.style.borderTop = 'solid 2px black';
        bar_places_value.style.borderBottom = 'solid 2px black';
        bar_places_value.style.borderRight = 'solid 2px black';
        bar_places_value.style.height = '24px';
        bar_places_value.style.backgroundColor = "coral";
    let bar_places_value_text = document.createElement("div");
        bar_places_value_text.style.borderTop = 'solid 2px black';
        bar_places_value_text.style.borderBottom = 'solid 2px black';
        bar_places_value_text.style.borderLeft = 'solid 2px black';
        bar_places_value_text.style.backgroundColor = "coral";
        bar_places_value_text.textContent = entry[1];
    let bar_occ_value = document.createElement("div");
        bar_occ_value.style.width = Number(entry[0])*3+'px';
        bar_occ_value.style.borderTop = 'solid 2px';
        bar_occ_value.style.borderBottom = 'solid 2px';
        bar_occ_value.style.height = '24px';
        bar_occ_value.style.backgroundColor = "aquamarine";
    let bar_occ_value_text = document.createElement("div");
        bar_occ_value_text.style.borderTop = 'solid 2px black';
        bar_occ_value_text.style.borderBottom = 'solid 2px black';
        bar_occ_value_text.style.borderLeft = 'solid 2px black';
        bar_occ_value_text.style.backgroundColor = "aquamarine";
        bar_occ_value_text.textContent = entry[0];
    let bar_places_names = document.createElement("div");
        bar_places_names.className = "place_names";
        bar_places_names.style.borderTop = 'solid 2px black';
        bar_places_names.style.borderBottom = 'solid 2px black';
        bar_places_names.style.borderRight = 'solid 2px black';
        bar_places_names.style.height = '24px';
        bar_places_names.style.backgroundColor = 'seashell';
        bar_places_names.textContent = bars_names[entry[0]][0].replace('#','').replaceAll('_',' ') + (bars_names[entry[0]].length > 1 ? ' …' : '');
    bar.appendChild(bar_occ_value_text);
    bar.appendChild(bar_occ_value);
    bar.appendChild(bar_places_value_text);
    bar.appendChild(bar_places_value);
    bar.appendChild(bar_places_names);
    if (bars_names[entry[0]].length > 1) {
      bars_names[entry[0]].shift();
      let bar_places_names_more = document.createElement("div");/*
          bar_places_names_more.className = "place_names_more";
          bar_places_names_more.style.borderTop = 'solid 2px black';
          bar_places_names_more.style.borderBottom = 'solid 2px black';
          bar_places_names_more.style.borderRight = 'solid 2px black';
          bar_places_names_more.style.height = '24px';
          bar_places_names_more.style.backgroundColor = 'seashell';
          bar_places_names_more.textContent = '+';
      bar.appendChild(bar_places_names_more);*/
    }
    histogramme.appendChild(bar);
  }
  document.body.appendChild(histogramme);
}

function places_histogramme(titre_content,places,json) {
  let histogramme = document.createElement("section");
  histogramme.className = "place_histo";
 let titre = document.createElement("h3");
 titre.textContent = titre_content;
 histogramme.appendChild(titre);
  var occ_leg = document.createElement("p");
  occ_leg.style.color = "mediumaquamarine";
  occ_leg.className = "legend";
  occ_leg.style.fontSize = "1rem";
  occ_leg_content = 0;
  histogramme.appendChild(occ_leg);
  
  var leg = document.createElement("p");
  leg.className = "legend";
  leg.textContent = "Préliminaire";
  leg.style.fontSize = "1rem";
  leg.style.color = "coral";
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);

  let partie = 1;
  
  // preli
  let preli_bar = document.createElement("a");
      preli_bar.className = "bar";
      preli_bar.style.display = "flex";
      preli_bar.setAttribute("title",json.preliminary.name);
      preli_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
  let preli_bar_text = document.createElement("div");
      preli_bar_text.style.borderTop = 'solid 2px black';
      preli_bar_text.style.borderBottom = 'solid 2px black';
      preli_bar_text.style.borderLeft = 'solid 2px black';
      preli_bar_text.style.borderRight = 'solid 2px black';
      preli_bar_text.style.backgroundColor = "coral";
      preli_bar_text.style.width = "26px";
      //preli_bar_text.textContent = "Livre préliminaire"/*json.preliminary.name*/;
  let preli_bar_value = document.createElement("div");
      preli_bar_value.style.borderTop = 'solid 2px black';
      preli_bar_value.style.borderBottom = 'solid 2px black';
      preli_bar_value.style.borderRight = 'solid 2px black';
      preli_bar_value.style.backgroundColor = "coral";
      //preli_bar_value.style.width = 8*3+json.preliminary.places[place]*3+"px";
      //preli_bar_value.textContent = json.preliminary.places[place];
  preli_bar.appendChild(preli_bar_text);
  preli_bar.appendChild(preli_bar_value);
  histogramme.appendChild(preli_bar);

  var n = 0;
  for (var i in json.preliminary.chaps) {
    partie++;
    // preli_chaps
    let preli_chaps_bar = document.createElement("a");
        preli_chaps_bar.className = "bar";
        preli_chaps_bar.style.display = "flex";
        preli_chaps_bar.setAttribute("title",json.preliminary.chaps[i].name);
        preli_chaps_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
    let preli_chaps_bar_text = document.createElement("div");
        preli_chaps_bar_text.style.borderTop = 'solid 2px black';
        preli_chaps_bar_text.style.borderBottom = 'solid 2px black';
        preli_chaps_bar_text.style.borderLeft = 'solid 2px black';
        preli_chaps_bar_text.style.borderRight = 'solid 2px black';
        preli_chaps_bar_text.style.marginLeft = '18px';
        preli_chaps_bar_text.style.backgroundColor = "seashell";
        preli_chaps_bar_text.style.width = "8px";
    if (json.preliminary.chaps[i].places[place]) {
        //preli_chaps_bar_text.textContent = "chapitre " + (n+1)/*json.preli_chapsminary.name*/;
    }
    let preli_chaps_bar_value = document.createElement("div");
        preli_chaps_bar_value.style.borderTop = 'solid 2px black';
        preli_chaps_bar_value.style.borderBottom = 'solid 2px black';
        preli_chaps_bar_value.style.borderRight = 'solid 2px black';
        
        let width = 0;
        for (var place = 0; place < places.length; place++) {
          let value = json.preliminary.chaps[i].places[places[place]];
          if (!value) continue;
          width += value;
          occ_leg_content += value;
        }
        preli_chaps_bar_value.style.backgroundColor = width*8 ? "aquamarine" : "seashell";

        preli_chaps_bar_value.style.width = 8+width*8-4+"px";
        //preli_chaps_bar_value.textContent = json.preliminary.chaps[i].places[place];
      preli_chaps_bar.appendChild(preli_chaps_bar_text);
      preli_chaps_bar.appendChild(preli_chaps_bar_value);
    histogramme.appendChild(preli_chaps_bar);
    n++;
  }
  var leg = document.createElement("p");
  leg.className = "legend";
  leg.textContent = "Œuvre";
  leg.style.color = "coral";
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);
  
  // oeuvre
  let oeuvre_bar = document.createElement("a");
      oeuvre_bar.className = "bar";
      oeuvre_bar.style.display = "flex";
      oeuvre_bar.setAttribute("title",json.oeuvre.name);
      partie++;
      oeuvre_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
  let oeuvre_bar_text = document.createElement("div");
      oeuvre_bar_text.style.borderTop = 'solid 2px black';
      oeuvre_bar_text.style.borderBottom = 'solid 2px black';
      oeuvre_bar_text.style.borderLeft = 'solid 2px black';
      oeuvre_bar_text.style.borderRight = 'solid 2px black';
      oeuvre_bar_text.style.backgroundColor = "coral";
      oeuvre_bar_text.style.width = "26px";
      //oeuvre_bar_text.textContent = "Oeuvre"/*json.oeuvreminary.name*/;
  let oeuvre_bar_value = document.createElement("div");
      oeuvre_bar_value.style.borderTop = 'solid 2px black';
      oeuvre_bar_value.style.borderBottom = 'solid 2px black';
      oeuvre_bar_value.style.borderRight = 'solid 2px black';
      oeuvre_bar_value.style.backgroundColor = "coral";
      //oeuvre_bar_value.style.width = 8*3+json.oeuvre.places[place]*3+"px";
      //oeuvre_bar_value.textContent = json.oeuvre.places[place];
  oeuvre_bar.appendChild(oeuvre_bar_text);
  oeuvre_bar.appendChild(oeuvre_bar_value);
  histogramme.appendChild(oeuvre_bar);
  
  var n = 0;
  for (var i in json.oeuvre.parts) {
    // preli_parts
    let oeuvre_parts_bar = document.createElement("a");
        oeuvre_parts_bar.className = "bar";
        oeuvre_parts_bar.style.display = "flex";
        oeuvre_parts_bar.setAttribute("title",json.oeuvre.parts[i].name);
        partie++;
        oeuvre_parts_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
    let oeuvre_parts_bar_text = document.createElement("div");
        oeuvre_parts_bar_text.style.borderTop = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderBottom = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderLeft = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderRight = 'solid 2px black';
        oeuvre_parts_bar_text.style.backgroundColor = "coral";
        oeuvre_parts_bar_text.style.marginLeft = "6px";
        oeuvre_parts_bar_text.style.width = "20px";
    if (json.oeuvre.parts[i].places[place]) {
        //oeuvre_parts_bar_text.textContent = "Partie " + (n+1)/*json.oeuvre_partsminary.name*/;
    }
    let oeuvre_parts_bar_value = document.createElement("div");
        oeuvre_parts_bar_value.style.borderTop = 'solid 2px black';
        oeuvre_parts_bar_value.style.borderBottom = 'solid 2px black';
        oeuvre_parts_bar_value.style.borderRight = 'solid 2px black';
        oeuvre_parts_bar_value.style.backgroundColor = "coral";
        //oeuvre_parts_bar_value.style.width = 8*3+json.oeuvre.parts[i].places[place]*3+"px";
        //oeuvre_parts_bar_value.textContent = json.oeuvre.parts[i].places[place];
    oeuvre_parts_bar.appendChild(oeuvre_parts_bar_text);
    oeuvre_parts_bar.appendChild(oeuvre_parts_bar_value);
    histogramme.appendChild(oeuvre_parts_bar);
    n++;
    var a = 0;
    for (var j in json.oeuvre.parts[i].livres) {
      // oeuvre_livres
      let oeuvre_livres_bar = document.createElement("a");
          oeuvre_livres_bar.className = "bar";
          oeuvre_livres_bar.style.display = "flex";
          oeuvre_livres_bar.setAttribute("title",json.oeuvre.parts[i].livres[j].name);
          partie++;
          oeuvre_livres_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
      let oeuvre_livres_bar_text = document.createElement("div");
          oeuvre_livres_bar_text.style.borderTop = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderBottom = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderLeft = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderRight = 'solid 2px black';
          oeuvre_livres_bar_text.style.backgroundColor = "coral";
          oeuvre_livres_bar_text.style.marginLeft = "12px";
          oeuvre_livres_bar_text.style.width = "14px";
      if (json.oeuvre.parts[i].livres[j].places[place]) {
          //oeuvre_livres_bar_text.textContent = "Livre " + (a+1)/*json.oeuvre_partsminary.name*/;
      }
      let oeuvre_livres_bar_value = document.createElement("div");
          oeuvre_livres_bar_value.style.borderTop = 'solid 2px black';
          oeuvre_livres_bar_value.style.borderBottom = 'solid 2px black';
          oeuvre_livres_bar_value.style.borderRight = 'solid 2px black';
          oeuvre_livres_bar_value.style.backgroundColor = "coral";
          //oeuvre_livres_bar_value.style.width = 8*3+json.oeuvre.parts[i].livres[j].places[place]*3+"px";
          //oeuvre_livres_bar_value.textContent = json.oeuvre.parts[i].livres[j].places[place];
      oeuvre_livres_bar.appendChild(oeuvre_livres_bar_text);
      oeuvre_livres_bar.appendChild(oeuvre_livres_bar_value);
      histogramme.appendChild(oeuvre_livres_bar);
      a++;
    var b = 0;
    for (var k in json.oeuvre.parts[i].livres[j].chaps) {
      // oeuvre_chaps
      let oeuvre_chaps_bar = document.createElement("a");
          oeuvre_chaps_bar.className = "bar";
          oeuvre_chaps_bar.style.display = "flex";
          oeuvre_chaps_bar.setAttribute("title",json.oeuvre.parts[i].livres[j].chaps[k].name);
          partie++;
          oeuvre_chaps_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
      let oeuvre_chaps_bar_text = document.createElement("div");
          oeuvre_chaps_bar_text.style.borderTop = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderBottom = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderLeft = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderRight = 'solid 2px black';
          oeuvre_chaps_bar_text.style.marginLeft = '18px';
          oeuvre_chaps_bar_text.style.backgroundColor = "seashell";
          oeuvre_chaps_bar_text.style.width = "6px";
      if (json.oeuvre.parts[i].livres[j].chaps[k].places[place]) {
          //oeuvre_chaps_bar_text.textContent = "Chapitre " + (b+1)/*json.oeuvre_partsminary.name*/;
      }
      let oeuvre_chaps_bar_value = document.createElement("div");
          oeuvre_chaps_bar_value.style.borderTop = 'solid 2px black';
          oeuvre_chaps_bar_value.style.borderBottom = 'solid 2px black';
          oeuvre_chaps_bar_value.style.borderRight = 'solid 2px black';
        
        let width = 0;
        for (var place = 0; place < places.length; place++) {
          let value = json.oeuvre.parts[i].livres[j].chaps[k].places[places[place]];
          if (!value) continue;
          width += value;
          occ_leg_content += value;
        }
          
          oeuvre_chaps_bar_value.style.backgroundColor = width ? "aquamarine" : "seashell";
          oeuvre_chaps_bar_value.style.width = 8+width*8-4+"px";
          //oeuvre_chaps_bar_value.textContent = json.oeuvre.parts[i].livres[j].chaps[k].places[place];
      oeuvre_chaps_bar.appendChild(oeuvre_chaps_bar_text);
      oeuvre_chaps_bar.appendChild(oeuvre_chaps_bar_value);
      histogramme.appendChild(oeuvre_chaps_bar);
      b++;
    }
    }
  }
  
  
  
 

  occ_leg.textContent = occ_leg_content + ' occurrences';
  document.body.appendChild(histogramme);
}

function character_histogramme(character,json) {
  let histogramme = document.createElement("section");
  histogramme.className = "character_histo";
  let titre = document.createElement("h3");
  titre.textContent = character.replace('#','').replaceAll('_',' ');
  histogramme.appendChild(titre);
  var leg = document.createElement("p");
  leg.style.color = "mediumbisque";
  leg.className = "legend";
  leg.textContent = json.characters[character] + ' occurrences';
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);
  
  var leg = document.createElement("p");
  leg.className = "legend";
  leg.textContent = "Préliminaire";
  leg.style.fontSize = "1rem";
  leg.className = "legend";
  leg.style.color = "coral";
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);
  
  // preli
  let preli_bar = document.createElement("a");
      preli_bar.className = "bar";
      preli_bar.style.display = "flex";
      preli_bar.setAttribute("title",json.preliminary.name);
      let partie = 1;
      preli_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
  let preli_bar_text = document.createElement("div");
      preli_bar_text.style.borderTop = 'solid 2px black';
      preli_bar_text.style.borderBottom = 'solid 2px black';
      preli_bar_text.style.borderLeft = 'solid 2px black';
      preli_bar_text.style.borderRight = 'solid 2px black';
      preli_bar_text.style.backgroundColor = "coral";
      preli_bar_text.style.width = "26px";
      //preli_bar_text.textContent = "Livre préliminaire"/*json.preliminary.name*/;
  let preli_bar_value = document.createElement("div");
      preli_bar_value.style.borderTop = 'solid 2px black';
      preli_bar_value.style.borderBottom = 'solid 2px black';
      preli_bar_value.style.borderRight = 'solid 2px black';
      preli_bar_value.style.backgroundColor = "coral";
      //preli_bar_value.style.width = 8*3+json.preliminary.characters[character]*3+"px";
      //preli_bar_value.textContent = json.preliminary.characters[character];
  preli_bar.appendChild(preli_bar_text);
  preli_bar.appendChild(preli_bar_value);
  histogramme.appendChild(preli_bar);

  var n = 0;
  for (var i in json.preliminary.chaps) {
    // preli_chaps
    let preli_chaps_bar = document.createElement("a");
        preli_chaps_bar.className = "bar";
        preli_chaps_bar.style.display = "flex";
        preli_chaps_bar.setAttribute("title",json.preliminary.chaps[i].name);
        partie++;
        preli_chaps_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
    let preli_chaps_bar_text = document.createElement("div");
        preli_chaps_bar_text.style.borderTop = 'solid 2px black';
        preli_chaps_bar_text.style.borderBottom = 'solid 2px black';
        preli_chaps_bar_text.style.borderLeft = 'solid 2px black';
        preli_chaps_bar_text.style.borderRight = 'solid 2px black';
        preli_chaps_bar_text.style.marginLeft = '18px';
        preli_chaps_bar_text.style.backgroundColor = "seashell";
        preli_chaps_bar_text.style.width = "8px";
    if (json.preliminary.chaps[i].characters[character]) {
        //preli_chaps_bar_text.textContent = "chapitre " + (n+1)/*json.preli_chapsminary.name*/;
    }
    let preli_chaps_bar_value = document.createElement("div");
        preli_chaps_bar_value.style.borderTop = 'solid 2px black';
        preli_chaps_bar_value.style.borderBottom = 'solid 2px black';
        preli_chaps_bar_value.style.borderRight = 'solid 2px black';
        preli_chaps_bar_value.style.backgroundColor = json.preliminary.chaps[i].characters[character]*8 ? "bisque" : "seashell";
        preli_chaps_bar_value.style.width = 8+json.preliminary.chaps[i].characters[character]*8-4+"px";
        //preli_chaps_bar_value.textContent = json.preliminary.chaps[i].characters[character];
      preli_chaps_bar.appendChild(preli_chaps_bar_text);
      preli_chaps_bar.appendChild(preli_chaps_bar_value);
    histogramme.appendChild(preli_chaps_bar);
    n++;
  }
  var leg = document.createElement("p");
  leg.className = "legend";
  leg.textContent = "Œuvre";
  leg.style.color = "coral";
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);
  
  // oeuvre
  let oeuvre_bar = document.createElement("a");
      oeuvre_bar.className = "bar";
      oeuvre_bar.style.display = "flex";
      oeuvre_bar.setAttribute("title",json.oeuvre.name);
      partie++;
      oeuvre_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
  let oeuvre_bar_text = document.createElement("div");
      oeuvre_bar_text.style.borderTop = 'solid 2px black';
      oeuvre_bar_text.style.borderBottom = 'solid 2px black';
      oeuvre_bar_text.style.borderLeft = 'solid 2px black';
      oeuvre_bar_text.style.borderRight = 'solid 2px black';
      oeuvre_bar_text.style.backgroundColor = "coral";
      oeuvre_bar_text.style.width = "26px";
      //oeuvre_bar_text.textContent = "Oeuvre"/*json.oeuvreminary.name*/;
  let oeuvre_bar_value = document.createElement("div");
      oeuvre_bar_value.style.borderTop = 'solid 2px black';
      oeuvre_bar_value.style.borderBottom = 'solid 2px black';
      oeuvre_bar_value.style.borderRight = 'solid 2px black';
      oeuvre_bar_value.style.backgroundColor = "coral";
      //oeuvre_bar_value.style.width = 8*3+json.oeuvre.characters[character]*3+"px";
      //oeuvre_bar_value.textContent = json.oeuvre.characters[character];
  oeuvre_bar.appendChild(oeuvre_bar_text);
  oeuvre_bar.appendChild(oeuvre_bar_value);
  histogramme.appendChild(oeuvre_bar);
  
  var n = 0;
  for (var i in json.oeuvre.parts) {
    // preli_parts
    let oeuvre_parts_bar = document.createElement("a");
        oeuvre_parts_bar.className = "bar";
        oeuvre_parts_bar.style.display = "flex";
        oeuvre_parts_bar.setAttribute("title",json.oeuvre.parts[i].name);
        partie++;
        oeuvre_parts_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
    let oeuvre_parts_bar_text = document.createElement("div");
        oeuvre_parts_bar_text.style.borderTop = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderBottom = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderLeft = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderRight = 'solid 2px black';
        oeuvre_parts_bar_text.style.backgroundColor = "coral";
        oeuvre_parts_bar_text.style.marginLeft = "6px";
        oeuvre_parts_bar_text.style.width = "20px";
    if (json.oeuvre.parts[i].characters[character]) {
        //oeuvre_parts_bar_text.textContent = "Partie " + (n+1)/*json.oeuvre_partsminary.name*/;
    }
    let oeuvre_parts_bar_value = document.createElement("div");
        oeuvre_parts_bar_value.style.borderTop = 'solid 2px black';
        oeuvre_parts_bar_value.style.borderBottom = 'solid 2px black';
        oeuvre_parts_bar_value.style.borderRight = 'solid 2px black';
        oeuvre_parts_bar_value.style.backgroundColor = "coral";
        //oeuvre_parts_bar_value.style.width = 8*3+json.oeuvre.parts[i].characters[character]*3+"px";
        //oeuvre_parts_bar_value.textContent = json.oeuvre.parts[i].characters[character];
    oeuvre_parts_bar.appendChild(oeuvre_parts_bar_text);
    oeuvre_parts_bar.appendChild(oeuvre_parts_bar_value);
    histogramme.appendChild(oeuvre_parts_bar);
    n++;
    var a = 0;
    for (var j in json.oeuvre.parts[i].livres) {
      // oeuvre_livres
      let oeuvre_livres_bar = document.createElement("a");
          oeuvre_livres_bar.className = "bar";
          oeuvre_livres_bar.style.display = "flex";
          oeuvre_livres_bar.setAttribute("title",json.oeuvre.parts[i].livres[j].name);
          partie++;
          oeuvre_livres_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
      let oeuvre_livres_bar_text = document.createElement("div");
          oeuvre_livres_bar_text.style.borderTop = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderBottom = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderLeft = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderRight = 'solid 2px black';
          oeuvre_livres_bar_text.style.backgroundColor = "coral";
          oeuvre_livres_bar_text.style.marginLeft = "12px";
          oeuvre_livres_bar_text.style.width = "14px";
      if (json.oeuvre.parts[i].livres[j].characters[character]) {
          //oeuvre_livres_bar_text.textContent = "Livre " + (a+1)/*json.oeuvre_partsminary.name*/;
      }
      let oeuvre_livres_bar_value = document.createElement("div");
          oeuvre_livres_bar_value.style.borderTop = 'solid 2px black';
          oeuvre_livres_bar_value.style.borderBottom = 'solid 2px black';
          oeuvre_livres_bar_value.style.borderRight = 'solid 2px black';
          oeuvre_livres_bar_value.style.backgroundColor = "coral";
          //oeuvre_livres_bar_value.style.width = 8*3+json.oeuvre.parts[i].livres[j].characters[character]*3+"px";
          //oeuvre_livres_bar_value.textContent = json.oeuvre.parts[i].livres[j].characters[character];
      oeuvre_livres_bar.appendChild(oeuvre_livres_bar_text);
      oeuvre_livres_bar.appendChild(oeuvre_livres_bar_value);
      histogramme.appendChild(oeuvre_livres_bar);
      a++;
    var b = 0;
    for (var k in json.oeuvre.parts[i].livres[j].chaps) {
      // oeuvre_chaps
      let oeuvre_chaps_bar = document.createElement("a");
          oeuvre_chaps_bar.className = "bar";
          oeuvre_chaps_bar.style.display = "flex";
          oeuvre_chaps_bar.setAttribute("title",json.oeuvre.parts[i].livres[j].chaps[k].name);
          partie++;
          oeuvre_chaps_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie);
      let oeuvre_chaps_bar_text = document.createElement("div");
          oeuvre_chaps_bar_text.style.borderTop = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderBottom = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderLeft = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderRight = 'solid 2px black';
          oeuvre_chaps_bar_text.style.marginLeft = '18px';
          oeuvre_chaps_bar_text.style.backgroundColor = "seashell";
          oeuvre_chaps_bar_text.style.width = "6px";
      if (json.oeuvre.parts[i].livres[j].chaps[k].characters[character]) {
          //oeuvre_chaps_bar_text.textContent = "Chapitre " + (b+1)/*json.oeuvre_partsminary.name*/;
      }
      let oeuvre_chaps_bar_value = document.createElement("div");
          oeuvre_chaps_bar_value.style.borderTop = 'solid 2px black';
          oeuvre_chaps_bar_value.style.borderBottom = 'solid 2px black';
          oeuvre_chaps_bar_value.style.borderRight = 'solid 2px black';
          oeuvre_chaps_bar_value.style.backgroundColor = json.oeuvre.parts[i].livres[j].chaps[k].characters[character] ? "bisque" : "seashell";
          oeuvre_chaps_bar_value.style.width = 8+json.oeuvre.parts[i].livres[j].chaps[k].characters[character]*8-4+"px";
          //oeuvre_chaps_bar_value.textContent = json.oeuvre.parts[i].livres[j].chaps[k].characters[character];
      oeuvre_chaps_bar.appendChild(oeuvre_chaps_bar_text);
      oeuvre_chaps_bar.appendChild(oeuvre_chaps_bar_value);
      histogramme.appendChild(oeuvre_chaps_bar);
      b++;
    }
    }
  }
  
  
  
 

  document.body.appendChild(histogramme);
}

function place_histogramme(place,json) {
  let histogramme = document.createElement("section");
  histogramme.className = "place_histo";
  let titre = document.createElement("h3");
  titre.textContent = place.replace('#','').replaceAll('_',' ');
  histogramme.appendChild(titre);
  var leg = document.createElement("p");
  leg.className = "legend";
  leg.style.color = "mediumaquamarine";
  leg.textContent = json.places[place] + ' occurrences';
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);
  
  var leg = document.createElement("p");
  leg.className = "legend";
  leg.textContent = "Préliminaire";
  leg.style.fontSize = "1rem";
  leg.style.color = "coral";
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);
  
  // preli
  let preli_bar = document.createElement("a");
      preli_bar.className = "bar";
      preli_bar.style.display = "flex";
      preli_bar.setAttribute("title",json.preliminary.name);
      let partie = 1;
      preli_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie+"&place="+place.replace('#','').replaceAll('_',' '));
  let preli_bar_text = document.createElement("div");
      preli_bar_text.style.borderTop = 'solid 2px black';
      preli_bar_text.style.borderBottom = 'solid 2px black';
      preli_bar_text.style.borderLeft = 'solid 2px black';
      preli_bar_text.style.borderRight = 'solid 2px black';
      preli_bar_text.style.backgroundColor = "coral";
      preli_bar_text.style.width = "26px";
      //preli_bar_text.textContent = "Livre préliminaire"/*json.preliminary.name*/;
  let preli_bar_value = document.createElement("div");
      preli_bar_value.style.borderTop = 'solid 2px black';
      preli_bar_value.style.borderBottom = 'solid 2px black';
      preli_bar_value.style.borderRight = 'solid 2px black';
      preli_bar_value.style.backgroundColor = "coral";
      //preli_bar_value.style.width = 8*3+json.preliminary.places[place]*3+"px";
      //preli_bar_value.textContent = json.preliminary.places[place];
  preli_bar.appendChild(preli_bar_text);
  preli_bar.appendChild(preli_bar_value);
  histogramme.appendChild(preli_bar);

  var n = 0;
  for (var i in json.preliminary.chaps) {
    // preli_chaps
    let preli_chaps_bar = document.createElement("a");
        preli_chaps_bar.className = "bar";
        preli_chaps_bar.style.display = "flex";
        preli_chaps_bar.setAttribute("title",json.preliminary.chaps[i].name);
        partie++;
        preli_chaps_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie+"&place="+place.replace('#','').replaceAll('_',' '));
    let preli_chaps_bar_text = document.createElement("div");
        preli_chaps_bar_text.style.borderTop = 'solid 2px black';
        preli_chaps_bar_text.style.borderBottom = 'solid 2px black';
        preli_chaps_bar_text.style.borderLeft = 'solid 2px black';
        preli_chaps_bar_text.style.borderRight = 'solid 2px black';
        preli_chaps_bar_text.style.marginLeft = '18px';
        preli_chaps_bar_text.style.backgroundColor = "seashell";
        preli_chaps_bar_text.style.width = "8px";
    if (json.preliminary.chaps[i].places[place]) {
        //preli_chaps_bar_text.textContent = "chapitre " + (n+1)/*json.preli_chapsminary.name*/;
    }
    let preli_chaps_bar_value = document.createElement("div");
        preli_chaps_bar_value.style.borderTop = 'solid 2px black';
        preli_chaps_bar_value.style.borderBottom = 'solid 2px black';
        preli_chaps_bar_value.style.borderRight = 'solid 2px black';
        preli_chaps_bar_value.style.backgroundColor = json.preliminary.chaps[i].places[place]*8 ? "aquamarine" : "seashell";
        preli_chaps_bar_value.style.width = 8+json.preliminary.chaps[i].places[place]*8-4+"px";
        //preli_chaps_bar_value.textContent = json.preliminary.chaps[i].places[place];
      preli_chaps_bar.appendChild(preli_chaps_bar_text);
      preli_chaps_bar.appendChild(preli_chaps_bar_value);
    histogramme.appendChild(preli_chaps_bar);
    n++;
  }
  var leg = document.createElement("p");
  leg.className = "legend";
  leg.textContent = "Œuvre";
  leg.style.color = "coral";
  leg.style.fontSize = "1rem";
  histogramme.appendChild(leg);
  
  // oeuvre
  let oeuvre_bar = document.createElement("a");
      oeuvre_bar.className = "bar";
      oeuvre_bar.style.display = "flex";
      oeuvre_bar.setAttribute("title",json.oeuvre.name);
      partie++;
      oeuvre_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie+"&place="+place.replace('#','').replaceAll('_',' '));
  let oeuvre_bar_text = document.createElement("div");
      oeuvre_bar_text.style.borderTop = 'solid 2px black';
      oeuvre_bar_text.style.borderBottom = 'solid 2px black';
      oeuvre_bar_text.style.borderLeft = 'solid 2px black';
      oeuvre_bar_text.style.borderRight = 'solid 2px black';
      oeuvre_bar_text.style.backgroundColor = "coral";
      oeuvre_bar_text.style.width = "26px";
      //oeuvre_bar_text.textContent = "Oeuvre"/*json.oeuvreminary.name*/;
  let oeuvre_bar_value = document.createElement("div");
      oeuvre_bar_value.style.borderTop = 'solid 2px black';
      oeuvre_bar_value.style.borderBottom = 'solid 2px black';
      oeuvre_bar_value.style.borderRight = 'solid 2px black';
      oeuvre_bar_value.style.backgroundColor = "coral";
      //oeuvre_bar_value.style.width = 8*3+json.oeuvre.places[place]*3+"px";
      //oeuvre_bar_value.textContent = json.oeuvre.places[place];
  oeuvre_bar.appendChild(oeuvre_bar_text);
  oeuvre_bar.appendChild(oeuvre_bar_value);
  histogramme.appendChild(oeuvre_bar);
  
  var n = 0;
  for (var i in json.oeuvre.parts) {
    // preli_parts
    let oeuvre_parts_bar = document.createElement("a");
        oeuvre_parts_bar.className = "bar";
        oeuvre_parts_bar.style.display = "flex";
        oeuvre_parts_bar.setAttribute("title",json.oeuvre.parts[i].name);
        partie++;
        oeuvre_parts_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie+"&place="+place.replace('#','').replaceAll('_',' '));
    let oeuvre_parts_bar_text = document.createElement("div");
        oeuvre_parts_bar_text.style.borderTop = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderBottom = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderLeft = 'solid 2px black';
        oeuvre_parts_bar_text.style.borderRight = 'solid 2px black';
        oeuvre_parts_bar_text.style.backgroundColor = "coral";
        oeuvre_parts_bar_text.style.marginLeft = "6px";
        oeuvre_parts_bar_text.style.width = "20px";
    if (json.oeuvre.parts[i].places[place]) {
        //oeuvre_parts_bar_text.textContent = "Partie " + (n+1)/*json.oeuvre_partsminary.name*/;
    }
    let oeuvre_parts_bar_value = document.createElement("div");
        oeuvre_parts_bar_value.style.borderTop = 'solid 2px black';
        oeuvre_parts_bar_value.style.borderBottom = 'solid 2px black';
        oeuvre_parts_bar_value.style.borderRight = 'solid 2px black';
        oeuvre_parts_bar_value.style.backgroundColor = "coral";
        //oeuvre_parts_bar_value.style.width = 8*3+json.oeuvre.parts[i].places[place]*3+"px";
        //oeuvre_parts_bar_value.textContent = json.oeuvre.parts[i].places[place];
    oeuvre_parts_bar.appendChild(oeuvre_parts_bar_text);
    oeuvre_parts_bar.appendChild(oeuvre_parts_bar_value);
    histogramme.appendChild(oeuvre_parts_bar);
    n++;
    var a = 0;
    for (var j in json.oeuvre.parts[i].livres) {
      // oeuvre_livres
      let oeuvre_livres_bar = document.createElement("a");
          oeuvre_livres_bar.className = "bar";
          oeuvre_livres_bar.style.display = "flex";
          oeuvre_livres_bar.setAttribute("title",json.oeuvre.parts[i].livres[j].name);
          partie++;
          oeuvre_livres_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie+"&place="+place.replace('#','').replaceAll('_',' '));
      let oeuvre_livres_bar_text = document.createElement("div");
          oeuvre_livres_bar_text.style.borderTop = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderBottom = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderLeft = 'solid 2px black';
          oeuvre_livres_bar_text.style.borderRight = 'solid 2px black';
          oeuvre_livres_bar_text.style.backgroundColor = "coral";
          oeuvre_livres_bar_text.style.marginLeft = "12px";
          oeuvre_livres_bar_text.style.width = "14px";
      if (json.oeuvre.parts[i].livres[j].places[place]) {
          //oeuvre_livres_bar_text.textContent = "Livre " + (a+1)/*json.oeuvre_partsminary.name*/;
      }
      let oeuvre_livres_bar_value = document.createElement("div");
          oeuvre_livres_bar_value.style.borderTop = 'solid 2px black';
          oeuvre_livres_bar_value.style.borderBottom = 'solid 2px black';
          oeuvre_livres_bar_value.style.borderRight = 'solid 2px black';
          oeuvre_livres_bar_value.style.backgroundColor = "coral";
          //oeuvre_livres_bar_value.style.width = 8*3+json.oeuvre.parts[i].livres[j].places[place]*3+"px";
          //oeuvre_livres_bar_value.textContent = json.oeuvre.parts[i].livres[j].places[place];
      oeuvre_livres_bar.appendChild(oeuvre_livres_bar_text);
      oeuvre_livres_bar.appendChild(oeuvre_livres_bar_value);
      histogramme.appendChild(oeuvre_livres_bar);
      a++;
    var b = 0;
    for (var k in json.oeuvre.parts[i].livres[j].chaps) {
      // oeuvre_chaps
      let oeuvre_chaps_bar = document.createElement("a");
          oeuvre_chaps_bar.className = "bar";
          oeuvre_chaps_bar.style.display = "flex";
          oeuvre_chaps_bar.setAttribute("title",json.oeuvre.parts[i].livres[j].chaps[k].name);
          partie++;
          oeuvre_chaps_bar.setAttribute("href","https://cartohugo.elan-numerique.fr/?partie="+partie+"&place="+place.replace('#','').replaceAll('_',' '));
      let oeuvre_chaps_bar_text = document.createElement("div");
          oeuvre_chaps_bar_text.style.borderTop = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderBottom = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderLeft = 'solid 2px black';
          oeuvre_chaps_bar_text.style.borderRight = 'solid 2px black';
          oeuvre_chaps_bar_text.style.marginLeft = '18px';
          oeuvre_chaps_bar_text.style.backgroundColor = "seashell";
          oeuvre_chaps_bar_text.style.width = "6px";
      if (json.oeuvre.parts[i].livres[j].chaps[k].places[place]) {
          //oeuvre_chaps_bar_text.textContent = "Chapitre " + (b+1)/*json.oeuvre_partsminary.name*/;
      }
      let oeuvre_chaps_bar_value = document.createElement("div");
          oeuvre_chaps_bar_value.style.borderTop = 'solid 2px black';
          oeuvre_chaps_bar_value.style.borderBottom = 'solid 2px black';
          oeuvre_chaps_bar_value.style.borderRight = 'solid 2px black';
          oeuvre_chaps_bar_value.style.backgroundColor = json.oeuvre.parts[i].livres[j].chaps[k].places[place] ? "aquamarine" : "seashell";
          oeuvre_chaps_bar_value.style.width = 8+json.oeuvre.parts[i].livres[j].chaps[k].places[place]*8-4+"px";
          //oeuvre_chaps_bar_value.textContent = json.oeuvre.parts[i].livres[j].chaps[k].places[place];
      oeuvre_chaps_bar.appendChild(oeuvre_chaps_bar_text);
      oeuvre_chaps_bar.appendChild(oeuvre_chaps_bar_value);
      histogramme.appendChild(oeuvre_chaps_bar);
      b++;
    }
    }
  }
  
  
  
 

  document.body.appendChild(histogramme);
}

fetch("../../assets/json/stats.json")
.then(function(response) {
  return response.json();
})
.then(function(json) {
  var h2 = document.createElement("h2");
  h2.textContent = "Introduction";
  document.body.appendChild(h2);
  
  var p = document.createElement("p");
  p.textContent = "Sur cette page vous trouverez différents types d'histogrammes ayant comme entrées les données de lieux, personnages et structure chronologique du livre Les Travailleurs de la mer de Victor Hugo."
  document.body.appendChild(p);
  
  var h2 = document.createElement("h2");
  h2.textContent = "Index";
  document.body.appendChild(h2);
  var a = document.createElement("a");
  a.href = "#lieux_oeuvre";
  a.textContent = "Lieux de l'œuvre";
  document.body.appendChild(a);
  var a = document.createElement("a");
  a.href = "#lieux_chrono_oeuvre";
  a.textContent = "Lieux dans la chronologie de l'œuvre";
  document.body.appendChild(a);
  var a = document.createElement("a");
  a.href = "#perso_chrono_oeuvre";
  a.textContent = "Personnages dans la chronologie de l'œuvre";
  document.body.appendChild(a);
  var a = document.createElement("a");
  a.href = "#lieux_parts_oeuvre";
  a.textContent = "Lieux dans les différentes parties de l'œuvre";
  document.body.appendChild(a);
  
  var h2 = document.createElement("h2");
  h2.textContent = "Lieux de l'œuvre";
  h2.id = "lieux_oeuvre";
  document.body.appendChild(h2);
  var p = document.createElement("p");
  p.textContent = "Le graphique ci-dessous affiche dans l'ordre décroissant une comparaison entre une valeur d'occurrence de lieu et le nombre de lieux ayant cette valeur d'occurrence dans l'œuvre. La troisième donnée de la ligne est le nom du premier lieu de la liste ainsi formée."
  document.body.appendChild(p);
  var br = document.createElement("br");
  document.body.appendChild(br);
  var p = document.createElement("p");
  p.textContent = "À titre d'exemple la première ligne du graphique indique que Douvres est cité 203 fois  dans le texte (203 en vert) et c'est le seul lieu cité 203 fois (1 en rouge). À l'inverse, la dernière ligne indique que Ingouville est cité une seule fois dans le texte (1 en vert) parmis 191 autres lieux (191 en rouge)."
  document.body.appendChild(p);
  
  occ_histogramme(json.name,json.places,true);
  
  var h2 = document.createElement("h2");
  h2.textContent = "Lieux dans la chronologie de l'œuvre";
  h2.id = "lieux_chrono_oeuvre";
  document.body.appendChild(h2);
  var p = document.createElement("p");
  p.innerHTML = "Les graphiques ci-dessous illustrent les <span style=\"color:mediumaquamarine\">occurrences</span> d'un lieu spécifique dans la chronologie de l'œuvre décrite selon ses <span style=\"color:coral\">parties</span> (<span style=\"color:coral\">partie</span>, <span style=\"color:coral\">livre</span>, chapitre etc.) affichées de haut en bas.";
  document.body.appendChild(p);
  
  get_entries(json,json.name,json.places);
  
  var h2 = document.createElement("h2");
  h2.textContent = "Personnages dans la chronologie de l'œuvre";
  h2.id = "perso_chrono_oeuvre";
  document.body.appendChild(h2);
  var p = document.createElement("p");
  p.textContent = "Les graphiques ci-dessous illustrent la présence d'un personnage dans la chronologie de l'œuvre. Les données de personnages sont ici très approximatives. Seules les occuences des noms complets ont été pris en compte. À titre d'exemple pour le personnage d'Ebenezer Caudray, seules les occurrences du nom complet \"Ebenezer Caudray\" sont prises en compte, excluant \"Ebenezer\" ou \"Caudray\" seuls. De plus ces graphiques ne prennent pas en compte lorsqu'un personnage est désigné autrement que par son nom complet. Par exemple l'apellation \"Miss Lethierry\" pour le personnage de Déruchette n'est pas prise en compte."
  document.body.appendChild(p);
  
  character_histogramme("#Gilliatt",json);
  character_histogramme("#Deruchette",json);
  character_histogramme("#LaDurande",json);
  character_histogramme("#MessLethierry",json);
  character_histogramme("#Rantaine",json);
  character_histogramme("#SieurClubin",json);
  character_histogramme("#EbenezerCaudray",json);
  
  var h2 = document.createElement("h2");
  h2.textContent = "Lieux dans les différentes parties de l'œuvre";
  h2.id = "lieux_parts_oeuvre";
  document.body.appendChild(h2);
  var p = document.createElement("p");
  p.textContent = "Les graphiques ci-dessous reprennent les mêmes modalités que celui des \"Lieux de l'œuvre\" mais cette fois appliquées indépendamment aux différentes parties de l'œuvre."
  document.body.appendChild(p);
  // for (var i in json.places) {
  // }
  occ_histogramme(json.preliminary.name,json.preliminary.places,true);
  //for (var i in json.preliminary.chaps) {
  //  occ_histogramme(json.preliminary.chaps[i].name,json.preliminary.chaps[i].places);
  //}
  //occ_histogramme(json.oeuvre.name,json.oeuvre.places,true);
  for (var i in json.oeuvre.parts) {
    occ_histogramme(json.oeuvre.parts[i].name,json.oeuvre.parts[i].places);
    //for (var j in json.oeuvre.parts[i].livres) {
    //  occ_histogramme(json.oeuvre.parts[i].livres[j].name,json.oeuvre.parts[i].livres[j].places);
    //  for (var k in json.oeuvre.parts[i].livres[j].chaps) {
    //    occ_histogramme(json.oeuvre.parts[i].livres[j].chaps[k].name,json.oeuvre.parts[i].livres[j].chaps[k].places);
    //  }
    //}
  }
});
