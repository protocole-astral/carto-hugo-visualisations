document.addEventListener("DOMContentLoaded",  function() {
    init();
}, false);

async function init() {

    // click sur les tabs pour montrer/cacher une cible
    var tabs = document.querySelectorAll(".tab_container .tab_item");
    tabs.forEach(tab => tab.addEventListener("click", handleTabClick));

    // click pr toggle
    var el = document.querySelector("#toggle_map");
    el.addEventListener("click", toggle_map);

    // click pr toggle nom des lieux
    var placenames_checkbox = document.querySelector("#place_names");
    placenames_checkbox.addEventListener("click", toggle_placenames);

    // resetZones("CG_1794.jpg");

    var imgToLocations = await getData("../../assets/json/imgToLocations.json");
    var stats = await getData("../../assets/json/stats.json");
    
    // pour filtrer les données :
    // var stats = stats.oeuvre;
    // var stats = stats.preliminary;

    appendZones("CG_1794.jpg", imgToLocations, stats);
    appendZones("CG_1781.jpg", imgToLocations, stats);
    appendZones("CG_1871.jpg", imgToLocations, stats);

}

function toggle_placenames() {
    var tab_target = document.querySelectorAll(".tab_target");
    tab_target.forEach(el => el.classList.toggle("placenames"));
}
function toggle_map() {
    var tab_target = document.querySelectorAll(".tab_target");
    tab_target.forEach(el => el.classList.toggle("img_carte"));
}

function resetZones(target) {
    var svg = document.querySelector("[data-target='" + target + "'] svg");
    var circles = svg.querySelectorAll("circle");
    for (var i = 0; i < circles.length; i++) {
        svg.removeChild(circles[i])
    }
}

function appendZones(target, imgToLocations, stats) {

    // get du tag svg correspondant à la target
    var svg = document.querySelector("[data-target='" + target + "'] svg");
    // get des zones pour la target
    var target_zones = imgToLocations[target];
    // get des stats pour le txt entier
    var data = stats.places;

    // retrait des zones non localisables sur une carte, ex : ["0", "0", "0", "0"]
    var array_zones = Object.entries(target_zones);
    var localizable_zones = array_zones.filter(function(zones) {
        var coordinates_sum = zones[1][0].reduce((partialSum, a) => partialSum + parseInt(a), 0);
        return coordinates_sum;
    });
    var localizable_zones = Object.fromEntries(localizable_zones);

    // retrait des data qui concernent des zones non localisables sur une carte
    var array_data = Object.entries(data);
    var localizable_data = array_data.filter(function(datum) {
        var place_ref = datum[0].replace('#','');
        return localizable_zones[place_ref];
    });
    var localizable_data = Object.fromEntries(localizable_data);

    var array_occ = Object.values(localizable_data);
    var occ_max = Math.max(...array_occ);
    
    var g_circle = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    var g_text = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g_text.id = "g_text";
    for (var [zone, coordinates] of Object.entries(localizable_zones)) {

        var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');

        if (target == "CG_1871.jpg") {
            var ratio = 4.1;
        } else {
            var ratio = 1;
        }

        circle.setAttribute('cx', coordinates[0][0] / ratio);
        circle.setAttribute('cy', coordinates[0][1] / ratio);

        var occ = localizable_data["#" + zone];

        // si on a de la donnée pr ce corresp
        if (occ != undefined) {

            var num = scale(occ, 1, occ_max, 20, 150);

            if (unknown.includes(zone)) {
                var color = "coral";
            } else {
                var color = "aquamarine";
            }
            circle.setAttribute('r', num / ratio);
            circle.setAttribute('fill', color);
            circle.setAttribute('stroke', "black");
            circle.setAttribute('stroke-width', 10 / ratio);

            var title = document.createElementNS('http://www.w3.org/2000/svg', 'title');
            title.textContent = zone.replaceAll('_',' ') + " – " + occ + " occ.";
            circle.appendChild(title);

            var circle_text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
            circle_text.textContent = zone.replaceAll('_',' ');
            circle.appendChild(circle_text);
            g_circle.appendChild(circle);

            if (occ > 10) {
                var rect_text = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
                rect_text.setAttribute('x', (coordinates[0][0] / ratio) + (60 / ratio));
                rect_text.setAttribute('y', (coordinates[0][1] / ratio) + (-10 / ratio));
                rect_text.setAttribute('width', 600 / ratio);
                rect_text.setAttribute('height', 150 / ratio);
                rect_text.setAttribute('fill', '#ffffffb8');
                g_text.appendChild(rect_text);

                var circle_text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
                circle_text.textContent = zone.replaceAll('_',' ');
                circle_text.setAttribute('x', (coordinates[0][0] / ratio) + (80 / ratio));
                circle_text.setAttribute('y', (coordinates[0][1] / ratio) + (120 / ratio));
                g_text.appendChild(circle_text);
            }

        }
        svg.appendChild(g_circle);
        svg.appendChild(g_text);

    }
}

function scale (number, inMin, inMax, outMin, outMax) {
    /*
        interval de départ : outMin, outMax
        interval à atteindre : outMin, outMax
    */
    return (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

async function getData(path) {
    var options = {
        method: 'GET',
        mode: "cors"
    };
    var response = await fetch(path, options);
    var text = await response.text();
    var json = JSON.parse(text);
    return json;
}